# Free

Developed with Unreal Engine 5


Risketos:

- [x] Disparos fisicos que se replican
- [x] Tres tipos de enemigos con diferente vida, daño, velocidad y tamaño y escalado de estos por oleadas.
- [x] Menú de conexion con búsqueda de servidor.
- [x] Sistema para armas diferente.
- [x] Destrucción y reparación de puertas.
- [x] Partículas en disparo del arma.
- [x] Modo espectador.
- [ ] Emotes.
- [x] Sistema de puntuación con compra con puntos y drops de objetos del enemigo .
