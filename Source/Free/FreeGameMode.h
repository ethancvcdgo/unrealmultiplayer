// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "FreeGameMode.generated.h"

UCLASS(minimalapi)
class AFreeGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AFreeGameMode();
};



