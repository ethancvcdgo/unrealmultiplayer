// Fill out your copyright notice in the Description page of Project Settings.


#include "MultiGameInstance.h"
#include "OnlineSessionSettings.h"
#include "Kismet/GameplayStatics.h"

void UMultiGameInstance::Init() {
	Super::Init();
	if(IOnlineSubsystem* sub = IOnlineSubsystem::Get()) {
		SessionInterface = sub->GetSessionInterface();
		if(SessionInterface.IsValid()) {
			SessionInterface->OnCreateSessionCompleteDelegates.AddUObject(this, &UMultiGameInstance::OnCreateSessionComplete);
			SessionInterface->OnJoinSessionCompleteDelegates.AddUObject(this, &UMultiGameInstance::OnJoinSessionComplete);
			SessionInterface->OnFindSessionsCompleteDelegates.AddUObject(this, &UMultiGameInstance::OnFindSessionComplete);
		}
	}
}

void UMultiGameInstance::OnCreateSessionComplete(FName SessionName, bool Succeded) {
	if(Succeded) {
		UE_LOG(LogTemp, Warning, TEXT("Creacionado"));
		GetWorld()->ServerTravel("/Game/FirstPerson/Maps/FirstPersonMap?listen");
	}
}
void UMultiGameInstance::OnFindSessionComplete(bool Succeded) {
	if(Succeded) {
		UE_LOG(LogTemp, Warning, TEXT("sussis"));
		auto searchResult = SessionSearch->SearchResults;

		if(searchResult.Num()) {
			SessionInterface->JoinSession(0,FName("Pipo"),searchResult[0]);
		}
	}
}
void UMultiGameInstance::OnJoinSessionComplete(FName SessionName, EOnJoinSessionCompleteResult::Type Result) {
	UE_LOG(LogTemp, Warning, TEXT("jolin"));

	if(APlayerController* PlayerController = UGameplayStatics::GetPlayerController(GetWorld(),0)) {
		FString joinAddress {""};
		SessionInterface->GetResolvedConnectString(SessionName, joinAddress);
		if(joinAddress != "") {
			PlayerController->ClientTravel(joinAddress, ETravelType::TRAVEL_Absolute);
		}
	}
}

void UMultiGameInstance::CreateServer() {
	UE_LOG(LogTemp, Warning, TEXT("Create server"));

	FOnlineSessionSettings s;
	s.bAllowJoinInProgress = true;
	s.bIsDedicated = false;
	s.bIsLANMatch = IOnlineSubsystem::Get()->GetSubsystemName() == "NULL" ? true : false;
	s.bShouldAdvertise=true;
	s.bUsesPresence=true;
	s.NumPublicConnections=4;

	SessionInterface->CreateSession(0, FName("Pipo"), s);
}
void UMultiGameInstance::JoinServer() {
	UE_LOG(LogTemp, Warning, TEXT("Join server"));

	SessionSearch = MakeShareable(new FOnlineSessionSearch());
	SessionSearch->bIsLanQuery = IOnlineSubsystem::Get()->GetSubsystemName() == "NULL" ? true : false;
	SessionSearch->MaxSearchResults = 100;
	SessionSearch->QuerySettings.Set("SEARCH_PRESENCE", true, EOnlineComparisonOp::Equals);

	SessionInterface->FindSessions(0, SessionSearch.ToSharedRef());
}
void UMultiGameInstance::PlayAlone() {
	UE_LOG(LogTemp, Warning, TEXT("Lloron"));
	UGameplayStatics::OpenLevel(GetWorld(), "/Game/FirstPerson/Maps/FirstPersonMap", true);
}
