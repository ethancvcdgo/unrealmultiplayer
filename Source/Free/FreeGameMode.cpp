// Copyright Epic Games, Inc. All Rights Reserved.

#include "FreeGameMode.h"
#include "FreeCharacter.h"
#include "UObject/ConstructorHelpers.h"

AFreeGameMode::AFreeGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPerson/Blueprints/BP_FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

}
